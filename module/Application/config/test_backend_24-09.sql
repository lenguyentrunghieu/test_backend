-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 24, 2019 at 04:55 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test_backend`
--
CREATE DATABASE IF NOT EXISTS `test_backend` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `test_backend`;

-- --------------------------------------------------------

--
-- Table structure for table `tb_department`
--

CREATE TABLE `tb_department` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='department';

--
-- Dumping data for table `tb_department`
--

INSERT INTO `tb_department` (`id`, `name`) VALUES
(1, 'Manager');

-- --------------------------------------------------------

--
-- Table structure for table `tb_profile`
--

CREATE TABLE `tb_profile` (
  `staff_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `seat_id` int(11) DEFAULT NULL,
  `first_name` text COLLATE utf8_unicode_ci NOT NULL,
  `family_name` text COLLATE utf8_unicode_ci NOT NULL,
  `department_id` int(11) DEFAULT NULL,
  `leader_staff_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `story` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='profile';

--
-- Dumping data for table `tb_profile`
--

INSERT INTO `tb_profile` (`staff_id`, `seat_id`, `first_name`, `family_name`, `department_id`, `leader_staff_id`, `photo`, `address`, `phone`, `story`, `email`) VALUES
('SAI190802001', NULL, 'Laurent', 'Le', NULL, NULL, 'Laurent Le.png', '56 street 66 , quang 2, ho chi minh ville, vietnam', '+84.906107060', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_seat`
--

CREATE TABLE `tb_seat` (
  `id` int(11) NOT NULL,
  `locX` int(11) NOT NULL,
  `locY` int(11) NOT NULL,
  `seat_number` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `floor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_department`
--
ALTER TABLE `tb_department`
  ADD PRIMARY KEY (`id`);
  ADD UNIQUE `name` (`name`);
  
--
-- Indexes for table `tb_profile`
--
ALTER TABLE `tb_profile`
  ADD PRIMARY KEY (`staff_id`) USING BTREE,
  ADD UNIQUE KEY `seat` (`seat_id`) USING BTREE,
  ADD KEY `department` (`department_id`),
  ADD KEY `leader` (`leader_staff_id`);

--
-- Indexes for table `tb_seat`
--
ALTER TABLE `tb_seat`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `floor_seat_number` (`floor`,`seat_number`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_department`
--
ALTER TABLE `tb_department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_seat`
--
ALTER TABLE `tb_seat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_profile`
--
ALTER TABLE `tb_profile`
  ADD CONSTRAINT `department` FOREIGN KEY (`department_id`) REFERENCES `tb_department` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `leader` FOREIGN KEY (`leader_staff_id`) REFERENCES `tb_profile` (`staff_id`) ON DELETE SET NULL,
  ADD CONSTRAINT `seat` FOREIGN KEY (`seat_id`) REFERENCES `tb_seat` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
