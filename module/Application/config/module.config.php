<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;
use Zend\Db\Adapter\AdapterInterface;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\ModuleManager\Feature\ConfigProviderInterface;

return [
    'router' => [
        'routes' => [
            // 'login' => [
            //     'type' => Literal::class,
            //     'options' => [
            //         'route'    => '/login',
            //         'defaults' => [
            //             'controller' => Controller\LoginController::class,
            //             'action'     => 'login',
            //         ],
            //     ],
            // ],
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'application' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/application[/:action]',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            //Department
            'department_api' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/api/department[/:id]',
                    'defaults' => [
                        'controller' => Controller\DepartmentRestfulController::class,
                    ],
                    'constraints' => [
                        'id' => '[a-zA-Z0-9_-]*',
                    ],
                ],
            ],
            'department_admin' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/admin/department[/:action][/:id]',
                    'defaults' => [
                        'controller' => Controller\DepartmentRestfulController::class,
                        'action'     => 'list',
                    ],
                    'constraints' => [
                        'id' => '[a-zA-Z0-9_-]*',
                    ],
                ],
            ],
            //Profile
            'profile_admin' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/admin/profile[/:action][/:id]',
                    'defaults' => [
                        'controller' => Controller\ProfileAdminController::class,
                        'action'     => 'list',
                    ],
                    'constraints' => [
                        'id' => '[a-zA-Z0-9_-]*',
                    ],
                ],
            ],
            'profile_api' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/api/profile[/:id]',
                    'defaults' => [
                        'controller' => Controller\ProfileRestfulController::class,
                    ],
                    'constraints' => [
                        'id' => '[a-zA-Z0-9_-]*',
                    ],
                ],
            ],
            //Seat
            'seat_api' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/api/seat[/:arg1][/:arg2]',
                    'defaults' => [
                        'controller' => Controller\SeatRestfulController::class,
                    ],
                    'constraints' => [
                        'arg1' => '[a-zA-Z0-9_-]*',
                        'arg2' => '[a-zA-Z0-9_-]*',
                    ],
                ],
            ],
            'seat_floor_api' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/api/seat/floor[/:floor_number]',
                    'defaults' => [
                        'controller' => Controller\SeatRestfulController::class,
                        'action' => 'floor'
                    ],
                    'constraints' => [
                        'floor_number' => '[a-zA-Z0-9_-]*',
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            //Department
            Model\DepartmentTable::class => function($container) {
                $tableGateway = $container->get(Model\DepartmentTableGateway::class);
                return new Model\DepartmentTable($tableGateway);
            },
            Model\DepartmentTableGateway::class => function ($container) {
                $dbAdapter = $container->get(AdapterInterface::class);
                $resultSetPrototype = new ResultSet();
                $resultSetPrototype->setArrayObjectPrototype(new Model\Department());
                return new TableGateway('tb_department', $dbAdapter, null, $resultSetPrototype);
            },
            //Profile
            Model\ProfileTable::class => function($container) {
                $tableGateway = $container->get(Model\ProfileTableGateway::class);
                return new Model\ProfileTable($tableGateway);
            },
            Model\ProfileTableGateway::class => function ($container) {
                $dbAdapter = $container->get(AdapterInterface::class);
                $resultSetPrototype = new ResultSet();
                $resultSetPrototype->setArrayObjectPrototype(new Model\Profile());
                return new TableGateway('tb_profile', $dbAdapter, null, $resultSetPrototype);
            },
            //Seat
            Model\SeatTable::class => function($container) {
                $tableGateway = $container->get(Model\SeatTableGateway::class);
                return new Model\SeatTable($tableGateway);
            },
            Model\SeatTableGateway::class => function ($container) {
                $dbAdapter = $container->get(AdapterInterface::class);
                $resultSetPrototype = new ResultSet();
                $resultSetPrototype->setArrayObjectPrototype(new Model\Seat());
                return new TableGateway('tb_seat', $dbAdapter, null, $resultSetPrototype);
            },
            OAuth2\PdoAdapter::class => function ($container) {
                $config = $container->get('config');

                if (empty($config['zf-oauth2']['db'])) {
                    throw new Exception\RuntimeException(
                        'The database configuration [\'zf-oauth2\'][\'db\'] for OAuth2 is missing'
                    );
                }
        
                $username = isset($config['zf-oauth2']['db']['username']) ? $config['zf-oauth2']['db']['username'] : null;
                $password = isset($config['zf-oauth2']['db']['password']) ? $config['zf-oauth2']['db']['password'] : null;
                $options  = isset($config['zf-oauth2']['db']['options']) ? $config['zf-oauth2']['db']['options'] : [];
        
                $oauth2ServerConfig = [];
                if (isset($config['zf-oauth2']['storage_settings'])
                    && is_array($config['zf-oauth2']['storage_settings'])
                ) {
                    $oauth2ServerConfig = $config['zf-oauth2']['storage_settings'];
                }
        
                return new OAuth2\PdoAdapter([
                    'dsn'      => $config['zf-oauth2']['db']['dsn'],
                    'username' => $username,
                    'password' => $password,
                    'options'  => $options,
                ], $oauth2ServerConfig);
            },
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
            Controller\LoginController::class => InvokableFactory::class,
            //Department
            Controller\DepartmentRestfulController::class => function($container) {
                    return new Controller\DepartmentRestfulController(
                        $container->get(Model\DepartmentTable::class)
                    );
                },
            //Profile
            Controller\ProfileAdminController::class => function($container) {
                    return new Controller\ProfileAdminController(
                        $container->get(Model\ProfileTable::class)
                    );
                },
            Controller\ProfileRestfulController::class => function($container) {
                    return new Controller\ProfileRestfulController(
                        $container->get(Model\ProfileTable::class)
                    );
                },
             //Seat
            Controller\SeatRestfulController::class => function($container) {
                    return new Controller\SeatRestfulController(
                        $container->get(Model\SeatTable::class)
                    );
                },
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy'
        ],
    ],
];
