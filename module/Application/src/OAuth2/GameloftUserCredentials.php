<?php 

namespace Application\OAuth2;

use OAuth2\Storage\UserCredentialsInterface;
use Zend\Http\Client as HttpClient;

class GameloftUserCredentials implements UserCredentialsInterface
{
    private $client;
    public function __construct()
    {
        $this->client = new HttpClient();
        $this->client->setAdapter('Zend\Http\Client\Adapter\Curl');
    }

    /**
     * Grant access tokens for basic user credentials.
     *
     * Check the supplied username and password for validity.
     *
     * You can also use the $client_id param to do any checks required based
     * on a client, if you need that.
     *
     * Required for OAuth2::GRANT_TYPE_USER_CREDENTIALS.
     *
     * @param $username
     * Username to be check with.
     * @param $password
     * Password to be check with.
     *
     * @return
     * TRUE if the username and password are valid, and FALSE if it isn't.
     * Moreover, if the username and password are valid, and you want to
     *
     * @see http://tools.ietf.org/html/rfc6749#section-4.3
     *
     * @ingroup oauth2_section_4
     */
    public function checkUserCredentials($username, $password)
    {
        $credentialType = 'ldap';

        $client = $this->client;

        $client->setUri('http://vbeta.gameloft.com:20000/locate/auth');
        $client->setMethod('GET');
        $response = $client->send();
        if (!$response->isSuccess()){
            return false;
        }

        $client->setUri('https://' . $response->getBody() . '/' . urlencode(strval($credentialType) . ':' . strval($username)) . '/authenticate');
        $client->setMethod('GET');
        $client->setParameterGET(array('password' => strval($password)));
        $response = $client->send();
        return $response->isSuccess();
    }

    /**
     * @param string $username - username to get details for
     * @return array|false     - the associated "user_id" and optional "scope" values
     *                           This function MUST return FALSE if the requested user does not exist or is
     *                           invalid. "scope" is a space-separated list of restricted scopes.
     * @code
     *     return array(
     *         "user_id"  => USER_ID,    // REQUIRED user_id to be stored with the authorization code or access token
     *         "scope"    => SCOPE       // OPTIONAL space-separated list of restricted scopes
     *     );
     * @endcode
     */
    public function getUserDetails($username)
    {
        return array(
            "user_id"  => $username,    // REQUIRED user_id to be stored with the authorization code or access token
             "scope"    => ""       // OPTIONAL space-separated list of restricted scopes
            );
    }
}