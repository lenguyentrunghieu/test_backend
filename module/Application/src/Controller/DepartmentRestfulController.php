<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Application\Model\DepartmentTable;
use Application\Model\Department;
use RuntimeException;
use Exception;
use Zend\View\Model\ViewModel;

class DepartmentRestfulController extends AbstractRestfulController
{
    private $departmentTable;

    public function __construct(DepartmentTable $table)
    {
        $this->departmentTable = $table;
    }
    
    /**
     * GET # ./api/department/{id}
     * params:
     *        id(path): int
     */
    public function get($id)
    {
        $response = $this->getResponseWithHeader();
        try {
            $results = $this->departmentTable->getDepartment($id);
            return $response->setContent(\Zend\Json\Json::encode($results));
        } catch(RuntimeException $e){
            return $response->setStatusCode(404);
        }
        
    }

    /**
     * GET # ./api/department
     *     params
     *         q(query): optional, search by name
     */
    public function getList()
    {
        $q = $this->getRequest()->getQuery('q');
        $response = $this->getResponseWithHeader();
        $results = $this->departmentTable->fetchAll($q);
        return $response->setContent(\Zend\Json\Json::encode($results));
    }

    /**
     * POST # ./api/department
     *     params
     *         id: 
     *         name: 
     */
    public function create($data)
    {
        $response = $this->getResponseWithHeader();
        try {
        
            $id = $this->params()->fromRoute('id');
            $data = isset($data) ? $data : array();
            if(empty($data['name']))
            {
                $query_name = $this->getRequest()->getQuery('name');
                if(!empty($query_name))
                {
                    $data['name'] = $query_name;
                }
            }
            if(empty($data['id']))
            {
                $query_name = $this->getRequest()->getQuery('id');
                if(!empty($query_name))
                {
                    $data['id'] = $query_name;
                }
            }
            
            
            if(empty($id)){
                $results = $this->departmentTable->insert($data);
            } else{
                $results = $this->departmentTable->update($id, $data);
            }
            return $response->setContent($results);
        } catch(Exception $e) {
            echo $e->getMessage();
            return $response->setStatusCode(400);
        }
    }

    /**
     * DELETE # ./api/department
     * params:
     *        id(query): int 
     */
    public function delete($id)
    {
        $response = $this->getResponseWithHeader();
        try{
            $results = $this->departmentTable->deleteDepartment($id);
            return $response->setContent($results);
        } catch(Exception $e) {
            echo $e->getMessage();
            return $response->setStatusCode(400);
        }
    }
    
    // configure response
    public function getResponseWithHeader()
    {
        $response = $this->getResponse();
        $response->getHeaders()
                 //make can accessed by *
                 ->addHeaderLine('Access-Control-Allow-Origin','*')
                 //set allow methods
                 ->addHeaderLine('Access-Control-Allow-Methods','POST PUT DELETE GET');

        return $response;
    }
    
    
    
    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $department = $this->departmentTable->getDepartment($id);
        return new ViewModel([
            'id' => $id,
            'department' => $department,
        ]);
    }
    
    public function addAction()
    {
        return new ViewModel();
    }
    
    public function listAction()
    {
        // grab the paginator from the ProfileTable
        $paginator = $this->departmentTable->fetchPaginator();
        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(5);

        return new ViewModel(array(
            'paginator' => $paginator
        ));
    }
    
    
    
}
