<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Application\Model\ProfileTable;
use Application\Model\Profile;
use RuntimeException;
use Exception;



class ProfileRestfulController extends AbstractRestfulController
{
    private $profileTable;

    public function __construct(ProfileTable $table)
    {
        $this->profileTable = $table;
    }
    
    /**
     * GET # ./api/profile/{id}
     * params:
     *        id(path): string
     */
    public function get($id)
    {
        $response = $this->getResponseWithHeader();
        try {
            $increase_rating =  $this->getRequest()->getQuery('increase_rating', false);
            if($increase_rating)
                $this->profileTable->increaseRating($id);
            
            $results = $this->profileTable->get($id);
            return $response->setContent(\Zend\Json\Json::encode($results));
        } catch(RuntimeException $e){
            echo $e->getMessage();
            return $response->setStatusCode(400);
        }
        
    }

    /**
     * GET # ./api/profile
     */
    public function getList()
    {
    
        $response = $this->getResponseWithHeader();
        $ids = $this->getRequest()->getQuery('ids', '');
        $sort_rating =  $this->getRequest()->getQuery('sort_rating', false);
        $q = $this->getRequest()->getQuery('q', '');
        $qo =  $this->getRequest()->getQuery('qo', '');
        $limit =  $this->getRequest()->getQuery('limit', -1);
        $offset =  $this->getRequest()->getQuery('offset', -1);
        if(!empty($qo))
        {
            try{
                $qo = \Zend\Json\Json::decode($qo);
            } catch(Exception $e) {
                echo "'qo' invalid";
                return $response->setStatusCode(400);
            }
        }
        else{
            $qo = array();
        }
        if(!empty($ids))
        {
            try{
                //$ids = \Zend\Json\Json::decode($ids);
                $ids = array_map('trim', explode(',', $ids));
            } catch(Exception $e) {
                echo "'ids' invalid";
                return $response->setStatusCode(400);
            }
        }
        else{
            $ids = array();
        }
        
        $response = $this->getResponseWithHeader();
        $results = $this->profileTable->fetchAll($sort_rating, $ids, $q, $qo, $limit, $offset);
        return $response->setContent(\Zend\Json\Json::encode($results));
    }
    
    /**
     * Create profile
     *     POST # ./api/profile
     *
     * Update profile 
     *     POST # ./api/profile/{id}
     *     
     * Params:
     *     staffId:
     *     seatId:
     *     firstName:
     *     lastName:
     *     departmentId:
     *     leaderId:
     *     photo:
     *     address:
     *     phone:
     *     story:
     *     email:
     *
     */
    public function create($data)
    {
        $response = $this->getResponseWithHeader();
        try {
        
            $id = $this->params()->fromRoute('id');
            $data = isset($data) ? $data : array();
            
            
            $photofile = $this->params()->fromFiles('photo');
            if(isset($photofile))
            {
                $data['photo'] = $photofile;
            }
            
            $data = Profile::parseRequestData($data);
            
            if(empty($id)){
                $results = $this->profileTable->insert($data);
            } else{
                $results = $this->profileTable->update($id, $data);
            }
            return $response->setContent($results);
        } catch(Exception $e) {
            echo $e->getMessage();
            return $response->setStatusCode(400);
        }
      
    }
    

    /**
     * DELETE # ./api/profile
     * params:
     *        id(query): int 
     */
    public function delete($id)
    {
        echo $id;
        try {
            $response = $this->getResponseWithHeader();
            $results = $this->profileTable->delete($id);
            return $response->setContent($results);
        } catch(Exception $e) {
            echo $e->getMessage();
            return $response->setStatusCode(400);
        }
    }
    
    // configure response
    public function getResponseWithHeader()
    {
        $response = $this->getResponse();
        $response->getHeaders()
                 //make can accessed by *
                 ->addHeaderLine('Access-Control-Allow-Origin','*')
                 //set allow methods
                 ->addHeaderLine('Access-Control-Allow-Methods','POST PUT DELETE GET');

        return $response;
    }
}
