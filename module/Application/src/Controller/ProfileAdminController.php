<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Http\Client as HttpClient;
use Zend\View\Model\ViewModel;

use Application\Model\ProfileTable;
use Application\Model\Profile;

class ProfileAdminController extends AbstractActionController
{
     private $profileTable;

    public function __construct(ProfileTable $table)
    {
        $this->profileTable = $table;
    }
    
    public function listAction()
    {
        // grab the paginator from the ProfileTable
        $paginator = $this->profileTable->fetchPaginator();
        // set the current page to what has been passed in query string, or to 1 if none set
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('page', 1));
        // set the number of items per page to 10
        $paginator->setItemCountPerPage(5);

        return new ViewModel(array(
            'paginator' => $paginator
        ));
    }
    
    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $profile = $this->profileTable->get($id);
        return new ViewModel([
            'id' => $id,
            'profile' => $profile,
        ]);
    }
    
    public function addAction()
    {
      return new ViewModel();
    }
}
