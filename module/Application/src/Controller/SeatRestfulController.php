<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Application\Model\SeatTable;
use Application\Model\Seat;
use RuntimeException;
use Exception;

class SeatRestfulController extends AbstractRestfulController
{
    private $seatTable;

    public function __construct(SeatTable $table)
    {
        $this->seatTable = $table;
        $this->setIdentifierName(null);
    }
    
    /**
     * GET # ./api/seat/floor/{floor_number}
     * GET # ./api/seat/floor
     */
    public function floorAction()
    {
        $floor_number = $this->params()->fromRoute('floor_number');
        $response = $this->getResponseWithHeader();
        try {
            
            if(empty($floor_number)){
                $results = $this->seatTable->getFloors();
            } else{
                $results = $this->seatTable->getSeatByFloor($floor_number);
            }
            
            return $response->setContent(\Zend\Json\Json::encode($results));
        } catch(RuntimeException $e){
            echo $e->getMessage();
            return $response->setStatusCode(404);
        }
    }

    /**
     * GET # ./api/seat/{arg1}/{arg2}
     * GET # ./api/seat
     */
    public function getList()
    {
        $arg1 = $this->params()->fromRoute('arg1');
        $arg2 = $this->params()->fromRoute('arg2');
        $response = $this->getResponseWithHeader();
        try {
            if(empty($arg1))
            {
                $results = $this->seatTable->fetchAll();
            }
            else
            {
                $results = $this->seatTable->getSeat($arg1,$arg2);
            }
            
            return $response->setContent(\Zend\Json\Json::encode($results));
        } catch(RuntimeException $e){
            echo $e->getMessage();
            return $response->setStatusCode(404);
        }
    }
    
    private static function parseDataSet(array $contentData)
    {
        $dataSet = array();
        if(!empty($contentData['id']))      $dataSet['id']              = $contentData['id'];
        if(!empty($contentData['floor']))   $dataSet['floor']           = $contentData['floor'];
        if(!empty($contentData['locX']))    $dataSet['locX']            = $contentData['locX'];
        if(!empty($contentData['locY']))    $dataSet['locY']            = $contentData['locY'];
        if(!empty($contentData['seat']))    $dataSet['seat_number']     = $contentData['seat'];
        
        return $dataSet;
    }
    
    private function parseQuery($contentData, $paramName)
    {
        if(empty($contentData[$paramName]))
        {
            $paramValue = $this->getRequest()->getQuery($paramName);
            if(!empty($paramValue))
            {
                $contentData[$paramName] = $paramValue;
            }
        }
        return $contentData;
    }

    /**
     * POST # ./api/seat/{idOrigin}
     * POST # ./api/seat
     *     params
     *         id: 
     *         floor: 
     *         locX: 
     *         locY: 
     *         seat: 
     */
    public function create($data)
    {
        $response = $this->getResponseWithHeader();
        try {
        
            $id = $this->params()->fromRoute('id');
            $data = isset($data) ? $data : array();
            
            $data = $this->parseQuery($data, 'id');
            $data = $this->parseQuery($data, 'floor');
            $data = $this->parseQuery($data, 'locX');
            $data = $this->parseQuery($data, 'locY');
            $data = $this->parseQuery($data, 'seat');

            $dataSet = SeatRestfulController::parseDataSet($data);
            
            if(empty($id)){
                $results = $this->seatTable->insert($dataSet);
            } else{
                $results = $this->seatTable->update($id, $dataSet);
            }
            return $response->setContent($results);
        } catch(Exception $e) {
            echo $e->getMessage();
            return $response->setStatusCode(400);
        }
    }

    /**
     * DELETE # ./api/seat/{id}
     * params:
     *        id(query): int 
     */
    public function deleteList($data)
    {
        $id = $this->params()->fromRoute('id');
        
        $response = $this->getResponseWithHeader();
        $results = $this->seatTable->deleteSeat($id);
        return $response->setContent($results);
    }
    
    // configure response
    public function getResponseWithHeader()
    {
        $response = $this->getResponse();
        $response->getHeaders()
                 //make can accessed by *
                 ->addHeaderLine('Access-Control-Allow-Origin','*')
                 //set allow methods
                 ->addHeaderLine('Access-Control-Allow-Methods','POST PUT DELETE GET');

        return $response;
    }
}
