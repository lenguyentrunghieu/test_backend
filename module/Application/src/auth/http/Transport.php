<?php

namespace Application\auth\http;

interface Transport
{
	/**
	 * Executes GET request with parameters and returns response
	 *
	 * @param string $url
	 * @param array $data
     * @param http\TransportOptions $transport_options
	 * @return http\HTTPResponse
	 * @throws http\HTTPException
	 */
	public function sendGet($url, $data = array(), $transport_options = NULL);

	/**
	 * Executes POST request with parameters and returns response
	 *
	 * @param string $url
	 * @param array $data
     * @param http\TransportOptions $transport_options
	 * @return http\HTTPResponse
	 * @throws http\HTTPException
	 */
	public function sendPost($url, $data = array(), $transport_options = NULL);
}
?>