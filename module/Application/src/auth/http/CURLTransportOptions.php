<?php

namespace Application\auth\http;

class CURLTransportOptions implements TransportOptions
{
    public $options = array();
    
    public function __construct ($options = array()) {
        if (count($options)>0) {
            foreach ($options as $name => $value) {
                $this->setOption($name, $value);
            }
        }
    }

    /**
     * @see http\TransportOptions::getOptions()
     * @return array
     */
    public function getOptions () {
        return $this->options;
    }
    
    /**
     * @see http\TransportOptions::getOption()
     * @param mixed $name
     * @return mixed
     * @throws Exception 
     */
    public function getOption ($name) {
        if (isset($this->options[$name])) {
            return $this->options[$name];
        } else {
            throw new \Exception("Option doesn't exist");
        }
    }
    
    /**
     * @see http\TransportOptions::setOption()
     * @param mmixed $name
     * @param mixed $value 
     */
    public function setOption ($name, $value) {
        $this->options[$name] = $value;
    }
    
    /**
     * @see http\TransportOptions::setOptions()
     * @param array $options 
     */
    public function setOptions ($options) {
        if (count($options)>0) {
            foreach ($options as $name => $value) {
                $this->setOption($name, $value);
            }
        }
    }
    
    /**
     * @see http\TransportOptions::mergeOptions()
     * @param CURLTransportOptions $transport_options 
     */
    public function mergeOptions ($transport_options) {
        if ($transport_options !== NULL) {
            $this->setOptions($transport_options->getOptions());
        }
    }
}
?>