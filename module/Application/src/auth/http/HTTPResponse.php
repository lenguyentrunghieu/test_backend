<?php

namespace Application\auth\http;

class HTTPResponse
{
	private $code;
	private $body;

	const SUCCESS = 200;
	const CREATED = 201;
	const ACCEPTED = 202;
	const MOVED = 301;
	const FOUND = 302;
	const NOT_MODIFIED = 304;
	const BAD_REQUEST = 400;
	const UNAUTHORIZED = 401;
    const FORBIDDEN = 403;
	const NOT_FOUND = 404;
    const METHOD_NOT_ALLOWED = 405;
	const CONFLICT = 409;
	const EXPECTATION_FAILED = 417;
	const INTERNAL_SERVER_ERROR = 500;
	const SERVICE_UNAVAILABLE = 503;

	/**
	 * @return boolean
	 */
	public function isSuccessful() {
		return self::isSuccessfulCode($this->code);
	}

	/**
	 * @param int $code
	 * @return boolean
	 */
	public static function isSuccessfulCode($code) {
		return in_array(
			$code,
			array(
				HTTPResponse::SUCCESS,
				HTTPResponse::CREATED,
				HTTPResponse::ACCEPTED,
			)
		);
	}

	public function __construct($code, $body = '') {
		$this->code = intval($code);
		$this->body = $body;
	}

	public function getCode() {
		return $this->code;
	}

	public function getBody() {
		return $this->body;
	}
}
?>