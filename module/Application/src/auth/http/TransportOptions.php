<?php

namespace Application\auth\http;

interface TransportOptions
{
    /**
     * Get an array of transport options
     * 
     * @return array 
     */
    public function getOptions();
    
    /**
     * Get an option by name
     * 
     * @param mixed $name
     * @return mixed
     */
    public function getOption($name);
    
    /**
     * Set an option
     * 
     * @param mixed $name
     * @param mixed $value
     */
    public function setOption($name, $value);
    
    /**
     * Set many options at once
     * 
     * @param array $options
     */
    public function setOptions ($options);
    
    /**
     * Merge current options object with another one. New object will override existant options.
     * 
     * @param TransportOptions $transport_options
     */
    public function mergeOptions ($transport_options);
}
?>