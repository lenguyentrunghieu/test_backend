<?php

namespace Application\auth\http;

//use http\HTTPException;
//use http\CURLTransportOptions;

class CURLTransport implements Transport {

    private static $defaultCurlOptions = array(
        CURLOPT_SSL_VERIFYPEER => false, // TODO get valid certificate
        CURLOPT_RETURNTRANSFER => true,
        //CURLOPT_TIMEOUT_MS => 500, // TODO measure default response time
        CURLOPT_TIMEOUT => 30, // TODO measure default response time

        CURLOPT_MAXREDIRS => 5,
        CURLOPT_FOLLOWLOCATION => true,
    );

    /**
     * @see http\Transport::sendGet()
     * @throws http\HTTPException
     */
    public function sendGet($url, $data = array(), $transport_options = NULL) {
        $curl_transport_options = new CURLTransportOptions(self::$defaultCurlOptions);
        $curl_transport_options->mergeOptions($transport_options);

        if (count($data) > 0) {
            $url .= '?' . http_build_query($data);
        }

        return $this->executeRequest($url, $curl_transport_options);
    }

    /**
     * @see http\Transport::sendPost()
     * @throws http\HTTPException
     */
    public function sendPost($url, $data = array(), $transport_options = NULL) {
        $curl_transport_options = new CURLTransportOptions(self::$defaultCurlOptions);
        $curl_transport_options->mergeOptions($transport_options);

        $curl_transport_options->setOption(CURLOPT_POST, true);
        $curl_transport_options->setOption(CURLOPT_POSTFIELDS, $data);
        
        return $this->executeRequest($url, $curl_transport_options);
    }

    /**
     * @param string $url
     * @param array $curlOptions
     * @return http\HTTPResponse
     * @throws http\HTTPException
     */
    private function executeRequest($url, CURLTransportOptions $curlOptions) {
        if ($curlOptions->getOption(CURLOPT_TIMEOUT) > self::$defaultCurlOptions[CURLOPT_TIMEOUT]) {
            set_time_limit($curlOptions->getOption(CURLOPT_TIMEOUT));
        }

        $curlHandle = curl_init($url);
        curl_setopt_array($curlHandle, $curlOptions->getOptions());

        $result = curl_exec($curlHandle);

        if ($errCode = curl_errno($curlHandle)) {
            $this->processCurlError($errCode, $curlHandle, $result);
        }

        $httpCode = curl_getinfo($curlHandle, CURLINFO_HTTP_CODE);

        curl_close($curlHandle);
        return new HTTPResponse($httpCode, $result);
    }

    /**
     * @param int $errCode
     * @param cURL handle $curlHandle
     * @param string $result
     * @throws http\HTTPException
     */
    private function processCurlError($errCode, $curlHandle, $result = '') {
        $error = "Error while trying to execute CURL request to: "
                . curl_getinfo($curlHandle, CURLINFO_EFFECTIVE_URL);
        $error .= "\n\tCurl error message: " . curl_error($curlHandle);
        $error .= "\n\tReceived response: " . $result;
        curl_close($curlHandle);
        throw new HTTPException($error, $errCode);
    }

}
?>