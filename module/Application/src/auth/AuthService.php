<?php

namespace Application\auth;

use Application\auth\http\Transport;
use Application\auth\http\CURLTransport;
use Application\auth\http\HTTPResponse;
use Application\auth\http\HTTPException;
use Application\crypt;

class AuthService
{
    /**
     * singleton
     */
    private static $s_AuthService = null;
    
    /**
	 * @var http\Transport
	 */
	private $transport;
    
    private function __construct() 
    {
		$this->transport =  new CURLTransport();
	}
    
    /**
	 * 
	 * @return Bool true
	 */
	private function checkAuthenticate($username, $password, $credentialType = 'ldap') 
    {
        $response_gethost = $this->transport->sendGET('http://vbeta.gameloft.com:20000/locate/auth');
        if(!$response_gethost->isSuccessful())
        {
            throw new HTTPException('AUTH HOST not found');
        }
		$response_auth = $this->transport->sendGET( 
			'https://' . $response_gethost->getBody() . '/' . urlencode(strval($credentialType) . ':' . strval($username)) . '/authenticate',
			array('password' => strval($password))
		);
        return $response_auth->isSuccessful();
	}
    
    public static function auth($url)
    {
        
        $current_id = session_id();
        if($current_id == '') {
            session_start();
        }
        
        $isLoginPage =(strpos(strrev($url), strrev('login')) === 0);
        
        $login_page =  get_router_url('login');
        $login_redirect = BASE_URL;
    
        if(!isset($_SESSION['username']))
        {
            if(isset($_POST['nameInputEmail']) && isset($_POST['nameInputPassword']))
            {
                $username = $_POST['nameInputEmail'];
                $password = $_POST['nameInputPassword'];
            }
            else if(isset($_COOKIE['username']) && isset($_COOKIE['password']))
            {
                $username = $_COOKIE['username'];
                $password = crypt\AesCtr::decrypt($_COOKIE['password'], 'any', 256);
            }
            
            
            if(isset($username) && isset($password))
            {
                if(AuthService::$s_AuthService == null)
                    AuthService::$s_AuthService = new AuthService();
                    
                try 
                {
                    if (AuthService::$s_AuthService->checkAuthenticate($username, $password, 'ldap')) 
                    {
                        $_SESSION['username'] = $username;
                        if(isset($_POST['nameChkRemember']))
                        {
                            /* Set cookie to last 1 year */
                            setcookie('username', $username, time()+60*60*24*365, '/');
                            setcookie('password', crypt\AesCtr::encrypt($password,  'any' , 256), time()+60*60*24*365, '/');
                        }
                    }
                    else
                    {
                        throw new HTTPException("Username or password are incorrect");
                    }
                } catch (HTTPException $ex) 
                {
                    $AUTH_ERROR = $ex->getMessage();
                }
            }
        }
        if(isset($_SESSION['username']))//login success
        {
            if($isLoginPage){
                header('Location:'  . $login_redirect);
                exit();
            }
        }
        else
        {
            if(!$isLoginPage)
            {
                AuthService::logout();
                $next_url = $login_page;// . '?next=' . urlencode($login_redirect);
                header('Location:' . $next_url);
                exit();
            }
            else if(isset($AUTH_ERROR))
            {
                return $AUTH_ERROR ;
            } 
        }
        
    }
    
    public static function logout()
    {
        unset($_SESSION['username']);
        setcookie('username', '', time() - 3600, '/');
        setcookie('password', '', time() - 3600, '/');
        setcookie('passwordv', '', time() - 3600, '/');
    }
    
    public static function isLoggedin()
    {
        $current_id = session_id();
        if($current_id == '') {
            session_start();
        }
        return isset($_SESSION['username']);
    }
    
    public static function getUsername()
    {
        $current_id = session_id();
        if($current_id == '') {
            session_start();
        }
        return isset($_SESSION['username']) ? $_SESSION['username'] : 'Guest';
    }
}

?>