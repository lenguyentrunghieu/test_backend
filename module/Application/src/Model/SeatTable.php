<?php

namespace Application\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Select;

/**
 * DB table: tb_seat
 */
class SeatTable
{
    
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $results = $this->tableGateway->select(function(Select $select){
        });
        $array_results = array();
        foreach( $results as $r )
            $array_results[] = $r;
        return $array_results;
    }

    public function getSeatByFloor($floor)
    {
        $results = $this->tableGateway->select(function(Select $select) use($floor){
            $select->where("floor = $floor");
        });
        $array_results = array();
        foreach( $results as $r )
            $array_results[] = $r;
        return $array_results;
    }
    
    public function getFloors()
    {
        $results = $this->tableGateway->select(function(Select $select){
            $select->columns(array("floor"));
            $select->group("floor");
        });
        $array_results = array();
        foreach( $results as $seat )
            $array_results[] = $seat->floor;
        return $array_results;
    }
    
    public function getSeat($arg1, $arg2)
    {
        if(!empty($arg1) && !empty($arg2))
        {
            $rowset = $this->tableGateway->select(['floor' => $arg1, 'seat_number' => $arg2]);
        }
        else if(!empty($arg1))
        {
            $rowset = $this->tableGateway->select(['id' => $arg1]);
        }
       
        if(isset($rowset))
            $row = $rowset->current();
            
        if (! $row) {
            throw new RuntimeException(sprintf(
                'Could not find seat'
            ));
        }

        return $row;
    }
    
    public function insert($data)
    {
       return $this->tableGateway->insert($data);
    }
    
    public function update($id, $data)
    {
        return $this->tableGateway->update($data, ['id' => $id]);
    }
        
    public function deleteSeat($id)
    {
        return $this->tableGateway->delete(['id' => (int) $id]);
    }
}
