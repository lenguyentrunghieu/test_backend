<?php

namespace Application\Model;

use Exception;
use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
 
/**
 * DB table: tb_profile
 */
class ProfileTable
{
    
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    //todo check performance with join > opt sql
    private static function makeJoin($select)
    {
        
        $select->join(array('leader' => 'tb_profile'), 'leader.staff_id = tb_profile.leader_staff_id', array('leaderFirstName' => 'first_name', 'leaderLastName' => 'family_name'), 'left');
        $select->join('tb_department', 'tb_department.id = tb_profile.department_id', array('departmentName' => 'name'), 'left');
        $select->join('tb_seat', 'tb_seat.id = tb_profile.seat_id', array('locX','locY','seat_number','floor'), 'left');
        return $select;
    }
    
    public function fetchPaginator()
    {
        // create a new Select object for the table profile
        $select = new Select('tb_profile');
        //$select->join(array('leader' => 'tb_profile'), 'leader.staff_id = tb_profile.leader_staff_id', array('leaderFirstName' => 'first_name', 'leaderLastName' => 'family_name'), 'left');
        //$select->join('tb_department', 'tb_department.id = tb_profile.department_id', array('departmentName' => 'name'), 'left');
        //$select->join('tb_seat', 'tb_seat.id = tb_profile.seat_id', array('locX','locY','seat_number','floor'), 'left');
        ProfileTable::makeJoin($select);
        
        // create a new result set based on the Profile entity
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Profile());
        // create a new pagination adapter object
        $paginatorAdapter = new DbSelect(
         // our configured select object
         $select,
         // the adapter to run it against
         $this->tableGateway->getAdapter(),
         // the result set to hydrate
         $resultSetPrototype
        );
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
    }
    
    public function fetchAll($sort_rating, array $ids, string $q, array $qo, $limit, $offset)
    {
        $results = $this->tableGateway->select(function(Select $select) use ($sort_rating, $ids, $q, $qo, $limit, $offset){
            
            //$select->join(array('leader' => 'tb_profile'), 'leader.staff_id = tb_profile.leader_staff_id', array('leaderFirstName' => 'first_name', 'leaderLastName' => 'family_name'), 'left');
            //$select->join('tb_department', 'tb_department.id = tb_profile.department_id', array('departmentName' => 'name'), 'left');
            //$select->join('tb_seat', 'tb_seat.id = tb_profile.seat_id', array('locX','locY','seat_number','floor'), 'left');
            ProfileTable::makeJoin($select);
            
            if(!empty($q))
            {
                if(count($qo) == 0)
                {//default
                    $qo[] = 'firstName';
                    $qo[] = 'lastName';
                }
                

                $select->where(function (Where $where) use ($ids, $q, $qo){
                   $where = $where->nest();
                   foreach ($qo as $column) 
                    {
                        if(!empty(PROFILE::MODEL_FIELD_TO_DB_COLUMN[$column]))
                        {
                            $column = PROFILE::MODEL_FIELD_TO_DB_COLUMN[$column];
                            $where->or->like("tb_profile.$column", "%$q%");
                        }
                    }
                   $where->unnest;
                });
                
            }
            
            if(count($ids) > 0)
            {
                $select->where(array("tb_profile.staff_id" => $ids),\Zend\Db\Sql\Predicate\PredicateSet::OP_AND);
            }
            
            if($sort_rating)
            {
                $select->order('tb_profile.rating DESC');
            }
            
            if($limit > 0)
                $select->limit($limit);   // always takes an integer/numeric
            if($offset >= 0)
                $select->offset($offset); // similarly takes an integer/numeric
            
            
            
            //echo $select->getSqlString();
            
        });
        
        $array_results = array();
        foreach( $results as $r ){
            $array_results[] = $r;
        }
        return $array_results;
    }

    public function increaseRating($staffId)
    {
        return $this->tableGateway->update(array('rating' => new Expression('rating + 1')), ['staff_id' => $staffId]);
    }
    
    public function get($staffId)
    {
        $rowset = $this->tableGateway->select(function(Select $select) use ($staffId){
            //$select->join(array('leader' => 'tb_profile'), 'leader.staff_id = tb_profile.leader_staff_id', array('leaderFirstName' => 'first_name', 'leaderLastName' => 'family_name'), 'left');
            //$select->join('tb_department', 'tb_department.id = tb_profile.department_id', array('departmentName' => 'name'), 'left');
            //$select->join('tb_seat', 'tb_seat.id = tb_profile.seat_id', array('locX','locY','seat_number','floor'), 'left');
            ProfileTable::makeJoin($select);
            $select->where(array('tb_profile.staff_id' => $staffId));
        });
        $row = $rowset->current();
        if (! $row) {
            throw new RuntimeException(sprintf(
                'Could not find row with identifier %s',
                $staffId
            ));
        }

        return $row;
    }

    public function insert($data)
    {
       return $this->tableGateway->insert($data);
    }
    
    public function update($staffId, $data)
    {
        return $this->tableGateway->update($data, ['staff_id' => $staffId]);
    }

    public function delete($staffId)
    {
        return $this->tableGateway->delete(['staff_id' => $staffId]);
    }
}
