<?php

namespace Application\Model;

/**
 * DB table: tb_seat
 */
class Seat
{
    
    /**
     * type: integer
     * db column: 'id'
     */
    public $id;
    
    /**
     * type: integer
     * db column: 'locX'
     */
    public $locX;
    
    /**
     * type: integer
     * db column: 'locY'
     */
    public $locY;
    
    /**
     * type: string
     * db column: 'seat_number'
     */
    public $seat;
    
    /**
     * type: integer
     * db column: 'floor'
     */
    public $floor;
    
    
    public function exchangeArray(array $data)
    {
        $this->id       = !empty($data['id']) ? $data['id'] : '';
        $this->locX     = !empty($data['locX']) ? $data['locX'] : '';
        $this->locY     = !empty($data['locY']) ? $data['locY'] : '';
        $this->seat     = !empty($data['seat_number']) ? $data['seat_number'] : '';
        $this->floor    = !empty($data['floor']) ? $data['floor'] : '';
    }
}
