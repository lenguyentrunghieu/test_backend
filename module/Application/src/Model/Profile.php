<?php

namespace Application\Model;


defined('APPLICATION_UPLOADS_DIR')
    || define('APPLICATION_UPLOADS_DIR', realpath(getcwd() . '/public/resource/photos/'));
    
/**
 * DB table: tb_profile
 */
class Profile
{
    
    	
	/**
	 * type: string
	 * db column: 'staff_id'
	 */
	public $staffId;
	
	/**
	 * type: integer
	 * db column: 'seat_id'
	 */
	public $seatId;
	
	/**
	 * type: string
	 * db column: 'first_name'
	 */
	public $firstName;
	
	/**
	 * type: string
	 * db column: 'family_name'
	 */
	public $lastName;
	
	/**
	 * type: int
	 * FOREIGN KEY
	 * db column: 'department_id'
	 */
	public $departmentId;
	
	/**
	 * type: int
	 * FOREIGN KEY
	 * db column: 'leader_staff_id'
	 */
	public $leaderId;
	
	/**
	 * type: string
	 * db column: 'photo'
	 */
	public $photo;
	
	/**
	 * type: string
	 * db column: 'address'
	 */
	public $address;
	
	/**
	 * type: string
	 * db column: 'phone'
	 */
	public $phone;
    
    /**
	 * type: string
	 * db column: 'story'
	 */
	public $story;
    
    /**
	 * type: string
	 * db column: 'email'
	 */
	public $email;
    
    /**
	 * type: int
	 * db column: 'rating'
	 */
	public $rating;
    
	public $leaderName;
	public $departmentName;
	public $locX, $locY, $seat, $floor;
    
    const DB_COLUMN_TO_MODEL_FIELD = array (
        'staff_id' => 'staffId',
        'seat_id' => 'seatId',
        'first_name' => 'firstName',
        'family_name' => 'lastName',
        'department_id' => 'departmentId',
        'photo' => 'photo',
        'address' => 'address',
        'phone' => 'phone',
        'story' => 'story',
        'email' => 'email',
        'rating' => 'rating',
    );
    const MODEL_FIELD_TO_DB_COLUMN = [
        'staffId' => 'staff_id',
        'seatId' => 'seat_id',
        'firstName' => 'first_name',
        'lastName' => 'family_name',
        'departmentId' => 'department_id',
        'photo' => 'photo',
        'address' => 'address',
        'phone' => 'phone',
        'story' => 'story',
        'email' => 'email',
        'rating' => 'rating',
    ];
    
    public static function parseRequestData($data)
    {
        $result = array();
        
        $uploadName = '';
        $file = $data['photo'];
        $data['photo'] = '';
        if(isset($file) && isset($file['name']))
        {
            $uploadName = uniqid() . '_' . $file['name'];
            $type=$file['type'];     
            $extensions=array('image/jpg','image/jpe','image/jpeg','image/jfif','image/png','image/bmp','image/dib','image/gif');
            if(in_array($type, $extensions)
                && move_uploaded_file($file['tmp_name'], APPLICATION_UPLOADS_DIR .'/'. $uploadName))
            {
                $data['photo'] = $uploadName;
            }
        }
        
        
        
        if(isset($data))
        {
            if(!empty($data['staffId']))        $result['staff_id']             = $data['staffId'];
            if(!empty($data['seatId']))         $result['seat_id']              = $data['seatId'];
            if(!empty($data['firstName']))      $result['first_name']           = $data['firstName'];
            if(!empty($data['lastName']))       $result['family_name']          = $data['lastName'];
            if(!empty($data['departmentId']))   $result['department_id']        = $data['departmentId'];
            if(!empty($data['leaderId']))       $result['leader_staff_id']      = $data['leaderId'];
            if(!empty($data['photo']))          $result['photo']                = $data['photo'];
            if(!empty($data['address']))        $result['address']              = $data['address'];
            if(!empty($data['phone']))          $result['phone']                = $data['phone'];
            if(!empty($data['story']))          $result['story']                = $data['story'];
            if(!empty($data['email']))          $result['email']                = $data['email'];
            if(!empty($data['rating']))         $result['rating']               = $data['rating'];
        }
        
        return $result;
    }
    
    public function exchangeArray(array $data)
    {
        $this->staffId          = !empty($data['staff_id']) ? $data['staff_id'] : '';
        $this->seatId           = !empty($data['seat_id']) ? $data['seat_id'] : '';
        $this->firstName        = !empty($data['first_name']) ? $data['first_name'] : '';
        $this->lastName         = !empty($data['family_name']) ? $data['family_name'] : '';
        $this->departmentId     = !empty($data['department_id']) ? $data['department_id'] : '';
        $this->leaderId         = !empty($data['leader_staff_id']) ? $data['leader_staff_id'] : '';
        $this->photo            = !empty($data['photo']) ? get_router_url('/resource/photos/' . $data['photo']) : '';
        $this->address          = !empty($data['address']) ? $data['address'] : '';
        $this->phone            = !empty($data['phone']) ? $data['phone'] : '';
        $this->story            = !empty($data['story']) ? $data['story'] : '';
        $this->email            = !empty($data['email']) ? $data['email'] : '';
        
        $this->leaderName           = (!empty($data['leaderFirstName']) ? $data['leaderFirstName'] : '') . ' ' . (!empty($data['leaderLastName']) ? $data['leaderLastName'] : '');
        $this->departmentName       = !empty($data['departmentName']) ? $data['departmentName'] : '';
        $this->locX                 = !empty($data['locX']) ? $data['locX'] : '';
        $this->locY                 = !empty($data['locY']) ? $data['locY'] : '';
        $this->seat                 = !empty($data['seat_number']) ? $data['seat_number'] : '';
        $this->floor                = !empty($data['floor']) ? $data['floor'] : '';
        $this->rating                = !empty($data['rating']) ? $data['rating'] : 0;
    }
    
}
