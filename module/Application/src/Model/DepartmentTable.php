<?php

namespace Application\Model;

use RuntimeException;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/**
 * DB table: tb_department
 */
class DepartmentTable
{
    
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    public function fetchPaginator()
    {
        // create a new Select object for the table department
        $select = new Select('tb_department');
        
        // create a new result set based on the Department entity
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new Department());
        // create a new pagination adapter object
        $paginatorAdapter = new DbSelect(
         // our configured select object
         $select,
         // the adapter to run it against
         $this->tableGateway->getAdapter(),
         // the result set to hydrate
         $resultSetPrototype
        );
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
    }
    
    public function fetchAll($q)
    {
        $results = $this->tableGateway->select(function(Select $select) use ($q){
            if(!empty($q))
            {
                $select->where("name like '%$q%'");
            }
        });
        $array_results = array();
        foreach( $results as $r )
            $array_results[] = $r;
        return $array_results;
    }

    public function getDepartment($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (! $row) {
            throw new RuntimeException(sprintf(
                'Could not find row with identifier %d',
                $id
            ));
        }

        return $row;
    }
    
    public function insert($data)
    {
       return $this->tableGateway->insert($data);
    }
    
    public function update($id, $data)
    {
        return $this->tableGateway->update($data, ['id' => $id]);
    }
        
    public function deleteDepartment($id)
    {
        return $this->tableGateway->delete(['id' => (int) $id]);
    }
}
