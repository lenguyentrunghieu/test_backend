<?php

namespace Application\Model;

/**
 * DB table: tb_department
 */
class Department
{
    
    /**
     * type: integer
     * db column: 'id'
     */
    public $id;
    
    /**
     * type: string
     * db column: 'name'
     */
    public $name;
    
    public function exchangeArray(array $data)
    {
        $this->id     = !empty($data['id']) ? $data['id'] : '';
        $this->name = !empty($data['name']) ? $data['name'] : '';
    }
}
